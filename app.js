
require("dotenv").config()
const bodyParser = require('body-parser')
const helmet = require('helmet')
const morgan = require('morgan');
const { sequelize } = require('./models/index');
const allowedMethods = require('./helpers/allowedMethods')
var createError = require('http-errors');
const express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');
const db = require("./models");
const app = express();
const routesv1 = require("./routes")
const apiErrorHandler = require("./error/api-error-handler")
const swaggerUi = require('swagger-ui-express');
const checkBatteryStatus = require ("./scheduledtasks/checkBatteryStatus");
const swaggerSpec = require("./routes/docs/configuration");

try {
    db.sequelize.sync()
    console.log("Database connected");
  } catch (e) {
    console.log("could not connect to database")
  }
app.use(express.json())
app.use(helmet()) // set security headers
app.use(cookieParser())
app.use(morgan('tiny'))
app.use(bodyParser.urlencoded({extended :true}));
app.use(bodyParser.json())
app.use(cors());
app.use(function (_req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'Content-Type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
app.use('/api/v1',allowedMethods,routesv1)
app.use(apiErrorHandler);

module.exports = app