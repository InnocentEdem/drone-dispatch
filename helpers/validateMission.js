

module.exports = function (data) {
  const requiredKeys = [
    "location",
    "title",
    "description",
    "et_depature",
    "et_delivery",
    "et_return",
    "payload",
    "drone_id"
  ];

  const keys = Object.keys(data);

  try {
    if (!keys?.length) {
      throw new Error("Request contains no Data");
    }
    const diff = requiredKeys.filter((e) => !keys.includes(e));

    if (diff?.length) {
      throw new Error(`Required fields are missing: [${diff}]`);
    }
    for (let e in data) {
      if (!data[e]) {
        throw new Error(`'${e}' value is null`);
      }
    }
  } catch (err) {
    return { error: true, message: err?.message };
  }
};