module.exports=function(state){

    const validState = ['IDLE','LOADING','LOADED','DELIVERING','DELIVERED','RETURNING']
    return validState.includes(state)

}