

module.exports = function (medication, drone, payload) {

    const medicationList = medication.map(element=>element?.dataValues)
    const droneData = drone.toJSON()
    const weightLimit = droneData?.weightLimit
    let payloadWeight = 0

    payload.forEach(element=>{
        for(let i of medicationList){
            if(i.id === element.medication_id){
                payloadWeight += (+element.quantity * +i.weight)
            }
        }

    })
  
  
    try {
      if (payloadWeight>weightLimit) {
        throw new Error(`Payload of ${payloadWeight} is heavier than Drone Max Capacity of ${weightLimit}`);
      }
    
    } catch (err) {
      return { error: true, message: err?.message };
    }
  };