const checkMedicationCode = require("./checkMedicationCode");

module.exports = function (data) {
  const code = data?.code;
  const expectedKeys = ["name", "weight", "code"];

  try {
    if (!data) {
      throw new Error(`No update Object on request body`)
    }
    const keys = Object.keys(data);

    if (!keys?.length) {
      throw new Error(`No property in update Object`)
    }

    for (const element of keys) {
      if (!expectedKeys.includes(element)) {
        throw new Error(`Unknown property in update Object`)
      }
      if (!data[element]) {
        throw new Error(`${element} property has no value`)
      }
    }

    if (code && !checkMedicationCode(code)) {
      throw new Error(`Medication Code contains illegal characters`);
    }
  } catch (err) {
    console.log(err);
    return { error: true, message: err.message };
  }
};
