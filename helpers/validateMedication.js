const checkMedicationCode = require("./checkMedicationCode");

module.exports = function (data) {
  const code = data?.code;
  const expectedKeys = ["name", "weight", "code"];

  try {
    if (!data) {
      throw new Error(`No Medication Object on request body`)
    }
    const keys = Object.keys(data);
    const diff = expectedKeys.filter((e) => !keys.includes(e));
    const extra = keys.filter((e) => !expectedKeys.includes(e));
    if (diff?.length) {
      throw new Error(`${diff} missing in request`);
    }
    if (extra?.length) {
      throw new Error(`Unexpected properties in request ,[${extra}]`);
    }
    const missingValues = [];
    Object.keys(data).forEach((element) => {
      if (!data?.[element]) {
        missingValues.push(element);
      }
    });
    if (missingValues?.length) {
      throw new Error(`No value for ${missingValues}`);
    }

    if (!checkMedicationCode(code)) {
      throw new Error(`Medication code contains illegal characters`);
    }
  } catch (err) {
    return { error: true, message: err.message };
  }
};
