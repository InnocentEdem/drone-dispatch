const checkModelName = require("../helpers/checkModelName");
const checkWeightLimit = require("../helpers/checkWeightLimit");
const checkBatteryRange = require("./checkBatteryRange");
const checkDroneState = require("./checkDroneState")
const models = ["Lightweight", "Middleweight", "CruiserWeight", "Heavyweight"]
const states = ['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING']


module.exports = function (drone) {
    try {
        if (!drone) throw new Error(`No 'newDrone' Object on request body`);

        const keysWithNoValues = ({} = {});
        Object.keys(drone).forEach((element) => {
            if (!drone?.[element]) {
                keysWithNoValues[element] = null;
            }
        });
        if (Object.keys(keysWithNoValues)?.length) {
            throw new Error(
                `Some required property(s) '${Object.keys(keysWithNoValues).map(
                    (e) => e
                )}' are missing values`
            );
        }
        const requiredProps = [
            "model",
            "weightLimit",
            "batteryCapacity",
            "registration",
        ];

        const availableProps = Object.keys(drone);
        const diff = requiredProps.filter((e) => !availableProps.includes(e));

        if (diff?.length) {
            throw new Error(`Drone data is missing '${[diff]}' property(s)`);
        }

        if (!checkModelName(drone?.model)) {
            throw new Error(`Model must be one of ${models}`);
        }

        if (!checkDroneState(drone?.state)) {
            throw new Error(`Drone State must be one of ${states}`);
        }

        if (!checkWeightLimit(drone?.weightLimit)) {
            throw new Error(`Weight Limit must be between 0 and 500`);
        }
        if (!checkBatteryRange(drone?.batteryCapacity)) {
            throw new Error(`Battery Capacity must be between 0 and 100`);
        }
    } catch (err) {
        return { error: true, message: err?.message };
    }
};
