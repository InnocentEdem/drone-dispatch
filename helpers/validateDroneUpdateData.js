const checkModelName = require("../helpers/checkModelName");
const checkWeightLimit = require("../helpers/checkWeightLimit");
const checkBatteryRange = require("./checkBatteryRange");
const checkDroneState = require("./checkDroneState")
const models = ["Lightweight", "Middleweight", "CruiserWeight", "Heavyweight"]
const states = ['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING']


module.exports = function (drone) {
    try {
        if (!drone) throw new Error(`No data in update request`);

        const keysWithNoValues = ({} = {});
        Object.keys(drone).forEach((element) => {
            if (!drone?.[element]) {
                keysWithNoValues[element] = null;
            }
        });
        if (Object.keys(keysWithNoValues)?.length) {
          throw new Error(
            `Some property(s) '${Object.keys(keysWithNoValues).map(
              (e) => e
            )}' are missing values`
          );
        }

        const availableProps = Object.keys(drone);

        if (availableProps.includes("model")) {
          if (!checkModelName(drone?.model)) {
            throw new Error(`Model must be one of ${models}`);
          }
        }
        if (availableProps.includes("state")) {
          if (!checkDroneState(drone?.state)) {
            throw new Error(`Drone State must be one of ${states}`);
          }
        }

        if (availableProps.includes("weightLimit")) {
          if (!checkWeightLimit(drone?.weightLimit)) {
            throw new Error(`Weight Limit must be between 0 and 500`);
          }
        }

        if (availableProps.includes("batteryCapacity")) {
          if (!checkBatteryRange(drone?.batteryCapacity)) {
            throw new Error(`Battery Capacity must be between 0 and 100`);
          }
        }


    } catch (err) {
        return { error: true, message: err?.message };
    }
};
