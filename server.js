const app = require('./app')
const { sequelize } = require('./models/index');
const checkBatteryStatus = require ("./scheduledtasks/checkBatteryStatus");



const PORT = 5001
checkBatteryStatus()
app.listen(PORT,()=>{
    console.log(`Server listening on Port ${PORT}`);
    sequelize.sync()
})
module.exports = app