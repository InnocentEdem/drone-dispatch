const ApiError = require("./ApiError")
function apiErrorHandler(err,_req,res,_next){
    console.log(err)

    if (err instanceof ApiError){
        return res.status(err.code).json({status:"failure",code:err.code,message:err.message, });
    }
    res.status(500).json("Something went wrong")
}
module.exports = apiErrorHandler
