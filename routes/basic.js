const express = require("express")
const {checkToken} = require("../middlewares/auth")
const {basicLevelAuth} = require("../middlewares/authRole")
const {update_medication} = require("../controllers/users/update_medication")
// const {delete_medication} = require("../controllers/users/delete_medication")
const {find_one_medication} = require("../controllers/users/find_one_medication")
const {find_all_medication} = require("../controllers/users/find_all_medication")
const {find_all_mission} = require("../controllers/users/find_all_mission")
const {find_one_mission} = require("../controllers/users/find_one_mission")
const { find_drone_missions } = require( "../controllers/users/find_drone_missions")
const { add_medication } = require("../controllers/users/add_medication")
const { find_booked_missions } = require("../controllers/users/find_booked_missions")
const { drone_booked_missions } = require("../controllers/users/drone_booked_missions")
const { loaded_medication } = require("../controllers/users/loaded_medication")
const {upload} = require("../middlewares/imageUpload")
const basicUserRouter = express.Router()

basicUserRouter.post("/basic/add-medication",checkToken, basicLevelAuth, upload.single('image'),add_medication )
basicUserRouter.put("/basic/medication/:id",checkToken, basicLevelAuth,update_medication )
basicUserRouter.get("/basic/medication/:id",checkToken, basicLevelAuth,find_one_medication  )
basicUserRouter.get("/basic/all-medication",checkToken, basicLevelAuth,find_all_medication )

basicUserRouter.get("/basic/all-missions",checkToken, basicLevelAuth,find_all_mission )
basicUserRouter.get("/basic/mission/:id",checkToken, basicLevelAuth,find_one_mission )
basicUserRouter.get("/basic/drone-missions/:id",checkToken, basicLevelAuth,find_drone_missions )
basicUserRouter.get("/basic/all-booked-missions",checkToken, basicLevelAuth,find_booked_missions )
basicUserRouter.get("/basic/drone-booked-missions/:droneId",checkToken, basicLevelAuth,drone_booked_missions )
basicUserRouter.get("/basic/loaded-medication/:droneId",checkToken, basicLevelAuth,loaded_medication)

module.exports = basicUserRouter