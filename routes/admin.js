const express = require("express")
const {create_new_user}= require("../controllers/admin/create_user")
const {add_new_drone} = require("../controllers/admin/add_new_drone")
const {update_drone_property} = require("../controllers/admin/update_drone_property")
const { get_battery_event_log } = require("../controllers/admin/get_battery_event_log")
const {checkToken} = require("../middlewares/auth")
const {adminLevelAuth} = require("../middlewares/authRole")
const adminRouter = express.Router()

adminRouter.post("/admin/create-user",checkToken, adminLevelAuth, create_new_user)
adminRouter.post("/admin/add-new-drone",checkToken,adminLevelAuth,add_new_drone)
adminRouter.put("/admin/update-drone-property/:droneId",checkToken,adminLevelAuth,update_drone_property)
adminRouter.get("/admin/battery-event-log/:page",checkToken,adminLevelAuth,get_battery_event_log)



module.exports = adminRouter