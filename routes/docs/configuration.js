const swaggerJSDoc = require('swagger-jsdoc');

const swaggerDefinition = {
  openapi: '3.0.3',

    info: {
        version: "1.0.0",
        title: "Drone Dispatch API Service",
        description:
          "A Dispatch API service to provide interaction and information flow between drones a clients.",
          contact: {
            email: "edem.kuwornu@protonmail.com"
        },
          license: {
          name: "MIT",
          url: "https://opensource.org/licenses/MIT",
        },
      },
     servers:[
      {url: "http://localhost:5001/api/v1"}
     ],
      components: {
        securitySchemes: {
            bearerAuth: {
                type: 'http',
                scheme: 'bearer',
                bearerFormat: 'JWT',
            }
        }
    },
    security: [{
        bearerAuth: []
    }],

    }

const options = {
  swaggerDefinition,
  apis: ['./**/*.yaml'],

}
module.exports = swaggerJSDoc(options);