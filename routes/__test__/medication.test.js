const app = require("../../app");
const request = require('supertest');
const should = require('should');
const db = require("../../models");
const supertest = require("supertest");
const { User,Medication } = require('../../models');
const credentials = require("./testData/credentials");
const drones = require("./testData/sampleDroneDetails")
const medications = require("./testData/sampleMedication")

const admin = credentials.admin
const droneDispatcher = credentials.testDispatcher2
const user = credentials.testBasicUser
const medication = medications.medication


describe('Add Medication', () => {
    const adminAuth = {};
    const basicAuth = {};

    beforeAll(async () => {
        // db.sequelize.sync()
        const response = await supertest(app).post("/api/v1/login").send({
            'email': admin.email,
            'password': admin.password
        })
        adminAuth.token = response?.body?.Data.accessToken
        await supertest(app).post('/api/v1/admin/create-user').send({
            ...user
        }).set("authorization", `bearer ${adminAuth.token}`)

    });

    beforeEach(async () => {
      const res = await supertest(app).post("/api/v1/login").send({
        email: user.email,
        password: user.password,
      });
      basicAuth.token = res?.body?.Data?.accessToken;
    });

    afterAll(async ()=>{
        await Medication.destroy({
            where :{
                code:medication.code
            }
        })
        await User.destroy({
            where:{
                email:user.email
            }
        })

    })

    it('returns 201 if a new Medication is created', async () => {

        const res = await request(app).post(`/api/v1/basic/add-medication`).send({
            medication: {
                ...medication
            }
        }).expect(201).set(
            "authorization", `bearer ${basicAuth.token}`
        )
        expect(res.body.message).toBe("Medication added")


    })
    it('returns 400 and Error Message if required Property is missing', async () => {
        const res = await request(app).post(`/api/v1/basic/add-medication`).send({
            medication: {
                ...medication,
                weight: ""
            }
        }).expect(400).set(
            "authorization", `bearer ${basicAuth.token}`
        )
        expect(res.body.message).toBe("No value for weight")
    })
    it('returns 400 and Error Message if Medication code contains a lowercase Character', async () => {
        const res = await request(app).post(`/api/v1/basic/add-medication`).send({
            medication: {
                ...medication,
                code: "cD-34"
            }
        }).expect(400).set(
            "authorization", `bearer ${basicAuth.token}`
        )
        expect(res.body.message).toBe("Medication code contains illegal characters")
    })

})