const app = require("../../app");
const request = require('supertest');
const should = require('should');
const db = require("../../models");
const supertest = require("supertest");
const { User,Drone } = require('../../models');
const credentials = require("./testData/credentials");
const drones = require("./testData/sampleDroneDetails")

const admin = credentials.admin
const droneDispatcher = credentials.testDispatcher


describe('Drone Registration', () => {
    const auth = {};
    const nonAdminAuth = {}

    beforeAll(async () => {
        // db.sequelize.sync()
        const response = await supertest(app).post("/api/v1/login").send({
            'email': admin.email,
            'password': admin.password
        })
        auth.token = response?.body?.Data.accessToken
        await supertest(app).post('/api/v1/admin/create-user').send({
            ...droneDispatcher
        }).set("authorization", `bearer ${auth.token}`)

        const res = await supertest(app).post("/api/v1/login").send({
            'email': droneDispatcher.email,
            'password': droneDispatcher.password
        })
        nonAdminAuth.token = res?.body?.Data?.accessToken
    });

    beforeEach(async()=>{
        const res = await supertest(app).post("/api/v1/login").send({
            'email': droneDispatcher.email,
            'password': droneDispatcher.password
        })
        nonAdminAuth.token = res?.body?.Data?.accessToken
    })

    afterAll(async()=>{
        await User.destroy({
            where:{
                email:droneDispatcher.email
            }
        })
        await Drone.destroy({
            where:{
                registration: drones.lightdrone.registration
            }
        })

    })

    it('returns 201 if a new drone is created', async () => {
        const lightdrone = drones.lightdrone
        const res = await request(app).post(`/api/v1/admin/add-new-drone`).send({
            newDrone: {
                ...lightdrone
            }
        }).expect(201).set(
            "authorization", `bearer ${auth.token}`
        )
        expect(res.body.message).toBe(`Drone created`)


    })
    it('returns 400 and Error Message if required Property is missing', async () => {
        const middledrone = drones.middledrone
        const res = await request(app).post(`/api/v1/admin/add-new-drone`).send({
            newDrone: {
                ...middledrone,
                weightLimit: ""
            }
        }).expect(400).set(
            "authorization", `bearer ${auth.token}`
        )
        expect(res.body.message).toBe(`Some required property(s) 'weightLimit' are missing values`)
    })

    it('returns 401 if account does not have admin role but attempts to add Drone', async () => {
        const middledrone = drones.middledrone
        const res = await request(app).post(`/api/v1/admin/add-new-drone`).send({
            newDrone: {
                ...middledrone,
            }
        }).expect(401).set(
            "authorization", `bearer ${nonAdminAuth.token}`
        )
    })
})

