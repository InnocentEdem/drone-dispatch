const app = require("../../app");
const request = require('supertest');
const should = require('should');
const db = require("../../models");
const supertest = require("supertest");
const {User} = require('../../models')
const teardown = require("./testUtils/teardown")




describe('User Creation and Login', () =>{

    const auth = {};
    const admindata = {
        email:'johndoe@dronemail.com',
        password:"Kofi1234"
    }
    const testDispatcher = {
        email: `jdispatcher@dronemail.com`,
        role:"dispatcher",
        password:"Kofi1234"
    }
    beforeAll( async()=>{ 
        // db.sequelize.sync()
        const response = await supertest(app).post("/api/v1/login").send({
            'email': 'johndoe@dronemail.com',
            'password':"Kofi1234"
        })
        auth.token = response?.body?.Data.accessToken
        
    });
    afterAll( async() => {
         return clearUser()
      });


    it("returns 200 if Admin is logged in", async () => {
        const res = await request(app).post('/api/v1/login').send({
           'email': 'johndoe@dronemail.com',
            'password':"Kofi1234"
        })

       expect(res.statusCode).toEqual(200);
        
    })


    it("returns 200 if a new user is created", async () => {

        const email = testDispatcher.email
        const role = testDispatcher.role
        const password = testDispatcher.password
        const res = await request(app).post('/api/v1/admin/create-user').send({
           'email': email,
           'role':role,
           'password':password
        }).expect(200).set(
            "authorization", `bearer ${auth.token}`
        )
        expect(res.body.message).toBe(`User: ${email} Role: ${role}`);
        
    })
    describe('Created User logs in',()=>{

        const userToken = {}
        beforeEach( async()=>{ 
            // db.sequelize.sync()
            const response = await supertest(app).post("/api/v1/login").send({
                'email': testDispatcher.email,
                'password':testDispatcher.password
            })
            userToken.token = response?.body?.Data.accessToken
            
        });


        it("returns 200 if the created user logs in", async () => {

            const email = testDispatcher.email
            const password = testDispatcher.password
            const res = await request(app).post('/api/v1/login').send({
               'email': email,
               'password':password
            }).expect(200).set(
                "authorization", `bearer ${userToken.token}`
            )
            expect(res.body.message).toBe("Login Successful");
            
        })

    })


    it("returns 400 if email of user being created already exists", async ()=>{
        const email = testDispatcher.email
        const role = 'basic'
        const password = 'mypassword'
        const res = await request(app).post('/api/v1/admin/create-user').send({
            'email': email,
            'role':role,
             'password':password
         }).expect(400).set(
             "authorization", `bearer ${auth.token}`
         )
         expect(res.body.message).toBe("User with Email already exists");


    })


    it("returns 400 if admin credential is wrong", async () => {
        const res = await request(app).post('/api/v1/login').send({
           'email': 'johndoe@dronemail.com',
            'password': Array.from(admindata.password).reverse().join('')
        }).expect(400)

       expect(res.statusCode).toEqual(400);
       expect(res.body.message).toBe("Username or Password does not exist")
        
    })
    const clearUser = async()=>{
        try{
            await User.destroy({
                where:{
                    email: testDispatcher?.email
                }
            })

        }catch(err){
            console.log(err);

        }

    }


});

