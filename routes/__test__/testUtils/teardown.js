const fs = require("fs");
const { sequelize } = require("../../../models");
const filepath = "../../../test_database.sqlite";

module.exports = async function () {

  let queryInterface = sequelize.getQueryInterface();
  queryInterface.sequelize.connectionManager.connections.default.close(); // manually close the sqlite connection which sequelize.close() omits
  sequelize.close();
  // fs.unlinkSync("test_db.sqlite");
  console.log("done!");
  process.exit(0);
};
