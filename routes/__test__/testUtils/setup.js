const db = require("../../../models")
const {User} = require("../../../models")
const bcrypt = require("bcrypt")

module.exports = async () => {
    
    console.log("I'll be called first before any test cases run......................................................");
    //add in what you need to do here
    process.env.NODE_ENV = 'test';
    await db.sequelize.sync()

    const email= 'johndoe@dronemail.com';
    const role= 'admin'
    const password=  bcrypt.hashSync('Kofi1234', 10)
    try{
             await User.findOrCreate({
            where:{email},
            defaults: {
                role, password,email
            },
        })

    }
    catch(err){
        console.log(err);
        return err
    }

}


