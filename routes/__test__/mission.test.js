const app = require("../../app");
const request = require("supertest");
const should = require("should");
const db = require("../../models");
const supertest = require("supertest");
const { User, Drone, Medication, Mission } = require("../../models");
const credentials = require("./testData/credentials");
const drones = require("./testData/sampleDroneDetails");
const medications = require("./testData/sampleMedication");
const missions = require("./testData/sampleMissions");

const admin = credentials.admin;
const droneDispatcher = credentials.testDispatcher3;
const basicUser = credentials.testBasicUser2;
const medication = medications.medication1;
const drone = drones.middledrone;
const mission = missions.mission1;

describe("Create Mission", () => {
  const dispatchAuth = {};
  const basicAuth = {};
  const adminAuth = {};
  let medData = {};
  let droneData = {};
  const missionDetails = {};

  beforeAll(async () => {
    // db.sequelize.sync()
    const response = await supertest(app).post("/api/v1/login").send({
      email: admin.email,
      password: admin.password,
    });

    adminAuth.token = response?.body?.Data.accessToken;

    await supertest(app)
      .post("/api/v1/admin/create-user")
      .send({
        ...droneDispatcher,
      })
      .set("authorization", `bearer ${adminAuth.token}`);

    await supertest(app)
      .post("/api/v1/admin/create-user")
      .send({
        ...basicUser,
      })
      .set("authorization", `bearer ${adminAuth.token}`);

    const medRes = await supertest(app)
      .post("/api/v1/basic/add-medication")
      .send({
        medication: {
          ...medication,
        },
      })
      .set("authorization", `bearer ${adminAuth.token}`);

    medData = { ...medRes?.body?.Data?.[0] };

    await supertest(app)
      .post("/api/v1/admin/add-new-drone")
      .send({
        newDrone: {
          ...drone,
        },
      })
      .set("authorization", `bearer ${adminAuth.token}`);

    const droneRes = await supertest(app)
      .get(`/api/v1/dispatch/drone-by-registration/${drone.registration}`)
      .set("authorization", `bearer ${adminAuth.token}`);
    droneData = { ...droneRes?.body?.Data };
  });

  beforeEach(async () => {
    const res = await supertest(app).post("/api/v1/login").send({
      email: droneDispatcher.email,
      password: droneDispatcher.password,
    });
    dispatchAuth.token = res?.body?.Data?.accessToken;
  });

  afterAll(async () => {
    await User.destroy({
      where: {
        email: droneDispatcher.email,
      },
    });
    await User.destroy({
      where: {
        email: basicUser.email,
      },
    });
    await Drone.destroy({
      where: {
        registration: drones.middledrone.registration,
      },
    });
    await Medication.destroy({
      where: {
        code: medication.code,
      },
    });
    await Mission.destroy({
      where: {
        id: missionDetails?.id,
      },
    });
  });

  it("returns 401 when a basic user tries to create a mission", async () => {
    await request(app)
      .post(`/api/v1/dispatch/new-mission`)
      .send({
        mission: {
          ...mission,
        },
      })
      .expect(401)
      .set("authorization", `bearer ${basicAuth.token}`);
  });

  it("returns mission details if mission is created", async () => {
    const res = await request(app)
      .post(`/api/v1/dispatch/new-mission`)
      .send({
        mission: {
          ...mission,
          drone_id: droneData.id,
          payload: [{ medication_id: medData.id, quantity: 3 }],
        },
      })
      .expect(201)
      .set("authorization", `bearer ${adminAuth.token}`);
    expect(res.body.message).toBe("Mission created");
    const newMission = res?.body?.Data;
    expect(+newMission?.drone_id).toBe(droneData?.id);
    expect(newMission.current_status).toBe("LOADING");
    expect(newMission.Medications).toBeTruthy();
    missionDetails.id = newMission?.id;
    missionDetails.drone_id = newMission.drone_id;
  });

  it("checks if the drone state changes to LOADING after mission is created", async () => {
    const res = await request(app)
      .get(`/api/v1/dispatch/drone/${missionDetails?.drone_id}`)
      .expect(200)
      .set("authorization", `bearer ${dispatchAuth.token}`);
    expect(res.body.message).toBe("Drone fetch successfull");
    expect(res?.body?.Data?.state).toBe("LOADING");
  });

  describe("Update mission and drone states", () => {
    it("returns 200 when drone state is updated to LOADED", async () => {
      const droneSetRes = await request(app)
        .patch(`/api/v1/dispatch/complete-loading/${missionDetails?.drone_id}`)
        .expect(200)
        .set("authorization", `bearer ${dispatchAuth.token}`);
      expect(droneSetRes?.body?.Data[0]?.state).toBe("LOADED");
    });

    it("returns 200 when Drone current mission status is updated to Loaded together with drone", async () => {
      const getMission = await request(app)
        .get(`/api/v1/basic/mission/${missionDetails?.id}`)
        .expect(200)
        .set("authorization", `bearer ${dispatchAuth.token}`);
      expect(getMission?.body?.Data.current_status).toBe("LOADED");
    });
  });
});
