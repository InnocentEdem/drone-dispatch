module.exports = {
  mission1: {
    location: "Takoradi",
    title: "Delivery Kwesimintsim Hospital",
    description: "Medical Supplies to assist in Emergency",
    et_depature: "18:00",
    et_delivery: "19:00",
    et_return: "18:00",
    drone_id: 4,
    payload: [
      { medication_id: 2, quantity: 3 },
      { medication_id: 1, quantity: 20 },
    ],
  },
  mission2: {
    location: "Keta",
    title: "Delivery to Keta Hospital",
    description: "Medical Supplies to assist in Emergency",
    et_depature: "18:00",
    et_delivery: "19:00",
    et_return: "18:00",
    drone_id: 4,
    payload: [
      { medication_id: 2, quantity: 3 },
      { medication_id: 1, quantity: 20 },
    ],
  },
  mission3: {
    location: "Battor",
    title: "Delivery to Battor Hospital",
    description: "Medical Supplies to assist in Emergency",
    et_depature: "18:00",
    et_delivery: "19:00",
    et_return: "18:00",
    drone_id: 4,
    payload: [
      { medication_id: 2, quantity: 3 },
      { medication_id: 1, quantity: 20 },
    ],
  },
};