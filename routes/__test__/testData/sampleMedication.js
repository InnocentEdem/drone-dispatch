module.exports = {
  medication: {
    name: "Zincovit",
    code: "ZC-35",
    weight: 35,
  },
  medication1: {
    name: "Paracetamol",
    code: "PM-25",
    weight: 25,
  },
  medication2: {
    name: "Chloramphetanol",
    code: "CL-80",
    weight: 80,
  },
  medication3: {
    name: "Amoxycilin 250",
    code: "AM-250",
    weight: 50,
  },
};