module.exports = {
  lightdrone: {
    registration: " 9G 450",
    model: "Lightweight",
    weightLimit: "200",
    batteryCapacity: "100",
    state: "IDLE",
  },
  heavydrone:{
    registration: " 9G 451",
    model: "Heavyweight",
    weightLimit: "500",
    batteryCapacity: "100",
    state: "IDLE",
  },
  middledrone:{
    registration: " 9G 451",
    model: "Middleweight",
    weightLimit: "300",
    batteryCapacity: "100",
    state: "IDLE",
  },
  cruiserdrone:{
    registration: " 9G 451",
    model: "Cruiserweight",
    weightLimit: "400",
    batteryCapacity: "100",
    state: "IDLE",
  },
};