const express = require("express");
const router = express.Router()
const adminRoute = require('./admin')
const loginRoute= require("./login")
const dispatchRoute = require("./dispatch")
const basicLevelRoute = require("./basic")

router.use(loginRoute)
router.use(adminRoute)
router.use(dispatchRoute)
router.use(basicLevelRoute)



module.exports = router