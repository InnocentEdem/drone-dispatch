const {defaults} = require('jest-config')

module.exports = async () => {
    return {
      verbose: true,
      globalSetup: "./routes/__test__/testUtils/setup.js",
      globalTeardown: './routes/__test__/testUtils/teardown.js'
    };
  };