'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Missions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      location: {
        type: Sequelize.STRING
      },
      title: {
        type: Sequelize.STRING
      },
      description: {
        type: Sequelize.TEXT
      },
      et_depature: {
        type: Sequelize.TEXT
      },
      DroneId: {
        references:{
          key:"id",
          model:"Drones"
         }
        },
      et_delivery: {
        type: Sequelize.TEXT
      },
      et_return: {
        type: Sequelize.TEXT
      },
      actual_depature_time: {
        type: Sequelize.TEXT
      },
      actual_delivery_time: {
        type: Sequelize.TEXT
      },
      actual_return_time: {
        type: Sequelize.TEXT
      },
      current_status: {
        type: Sequelize.STRING
      },
      drone_id: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Missions');
  }
};