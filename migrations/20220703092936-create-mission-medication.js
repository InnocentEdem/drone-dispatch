'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('MissionMedications', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      MedicationId: {
       references:{
        key:"id",
        model:"Medications"
       },
       type: Sequelize.INTEGER,
       onDelete: "CASCADE",
      },
     MissionId: {
        references:{
          key:"id",
          model:"Missions"
         },
         type: Sequelize.INTEGER,
         onDelete: "CASCADE",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('MissionMedications');
  }
};