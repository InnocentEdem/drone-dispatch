const path = require('path');

module.exports = {
    development: {
        username: 'root',
        password: 'root',
        storage: 'database.sqlite',
        dialect: 'sqlite',
    },
    testing: {
        username: 'root',
        password: 'root',
        storage: 'test_db.sqlite',
        dialect: 'sqlite',
    }
};