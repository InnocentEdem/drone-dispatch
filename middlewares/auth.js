const jwt = require("jsonwebtoken");
const ApiError = require ("../error/ApiError");

const checkToken = (req,res,next)=>{
    const authHeader = req.headers['authorization'];
    const token = authHeader && authHeader.split(' ')[1];    
 
    if(!token){
      next(ApiError.forbidden("unauthorized"))
      return 
    }
 
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
   
      if(err){
        next(ApiError.forbidden("unauthorized"))
        return 
      }
      req.user = user
      next()

    })


}
module.exports={checkToken}