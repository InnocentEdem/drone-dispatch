const ApiError = require("../error/ApiError");
const adminLevelAuth = (req, _res, next) => {
  const user = req?.user;

  if (!user) {
    next(ApiError.forbidden(`No authorization!`));
    return;
  }

  const role = user?.role;

  if (role === "admin") {
    next();
  } else {
    next(ApiError.forbidden(`No authorization!`));
  }
};
const dispatcherLevelAuth = (req, _res, next) => {
  try {
    const user = req?.user;
    if (!user) {
      next(ApiError.forbidden(`No authorization!`));
      return;
    }
    const { role } = user;
    if (role === "admin" || role === "dispatcher") {
      next();
    } else {
      next(ApiError.forbidden(`No authorization!`));
    }
  } catch (err) {
    console.log(err);
  }
};
const basicLevelAuth = (req, _res, next) => {
  try {
    const user = req?.user;
    if (!user) {
      next(ApiError.forbidden(`No authorization!`));
      return;
    }
    const { role } = user;
    if (role === "admin" || role === "dispatcher" || role === "basic") {
      next();
    } else {
      next(ApiError.forbidden(`No authorization!`));
    }
  } catch (err) {
    console.log(err);
  }
};

module.exports = {
  adminLevelAuth,
  dispatcherLevelAuth,
  basicLevelAuth,
};
