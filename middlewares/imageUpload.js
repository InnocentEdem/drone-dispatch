const multer = require('multer')



const storage = multer.diskStorage({
    destination: function (_req, _file, cb) {
      cb(null, '/imageAssets')
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
      const imageName = file.fieldname + '-' + uniqueSuffix
      req.imageName = imageName
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
  })

const upload = multer({
    limits: {
        fileSize:5242880
    },
   storage: storage
})

module.exports.upload = upload