'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Mission extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      Mission.belongsTo(models.Drone,{
        uniqueKey: 'DroneId'
      })

      Mission.belongsToMany(models.Medication,{
        through: models.MissionMedication,
        uniqueKey: 'MissionId'

      }) 
    }
  }
  Mission.init({
    location: DataTypes.STRING,
    title: DataTypes.STRING,
    description:DataTypes.TEXT,
    et_depature: DataTypes.TEXT,
    et_delivery: DataTypes.TEXT,
    et_return: DataTypes.TEXT,
    actual_depature_time: DataTypes.TEXT,
    actual_delivery_time: DataTypes.TEXT,
    actual_return_time: DataTypes.TEXT,
    current_status: DataTypes.STRING,
    drone_id:DataTypes.STRING
    
  }, {
    sequelize,
    modelName: 'Mission',
  });
  return Mission;
};