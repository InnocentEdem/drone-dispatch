'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class DroneStateEvents extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  DroneStateEvents.init({
    timestamp: DataTypes.TEXT,
    drone: DataTypes.TEXT,
    event: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'DroneStateEvents',
  });
  return DroneStateEvents;
};