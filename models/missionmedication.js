'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MissionMedication extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      // MissionMedication.belongsTo(models.Medication, {
      //   foreignKey: "medication_id",
      // })
      // MissionMedication.belongsTo(models.Mission, {
      //   foreignKey: "mission_id",
      // })
    }
  }
  MissionMedication.init({
    quantity: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'MissionMedication',
  });
  return MissionMedication;
};