const bcrypt = require('bcrypt');
const jwt = require("jsonwebtoken");
const { LoginService } = require("../services/login_service")
const ApiError = require("../error/ApiError")
const successObject = require('../helpers/successObject');
const User = new LoginService()


const user_login = async (req, res, next) => {

    const email = req?.body?.email
    const password = req?.body?.password
    if(!email || !password){
        next(ApiError.badRequest(`Login Credentials incomplete`))
        return
    }
    try {
        const userDetails = await User.loginUser({ email })
        if (!userDetails) {
             next(ApiError.badRequest(`Username or Password does not exist`))
            return
        }
        const isMatched = bcrypt.compareSync(password, userDetails.toJSON().password)

        if (!isMatched) {
            next(ApiError.badRequest(`Username or Password does not exist`))
            return
        }
        const user = {
            email,
            role: userDetails.toJSON().role
        }
        const accessToken = jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: "10000 s" }) //require('crypto').randomBytes(64).toString('hex')
        const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN);
        return res.
            json( successObject({ message: "Login Successful",data:{ accessToken, refreshToken}}))

    } catch (err) {
        console.log(err);
        res.status(401).json(err)
    }
}
module.exports.user_login = user_login