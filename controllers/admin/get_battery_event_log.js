const { AdminDroneService } = require("../../services/admin_drone_service");
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const successObject = require("../../helpers/successObject");
const ApiError = require("../../error/ApiError")
const db = new AdminDroneService()

async function get_battery_event_log (req, res, _next){
    const page = req?.params?.id
    const limit = 50;
    
    const offset = page? (page-1) * limit : undefined

    try{
       const response = await db.generateLog(offset,limit)
       const parsed = response.map(element=> element["battery_event"] = JSON.parse(element["battery_event"]))
       return res.status(200).json(successObject({ data: response, code: 200, message: "Generate battery audit log for all drones" }))

    }catch(err){
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.get_battery_event_log = get_battery_event_log