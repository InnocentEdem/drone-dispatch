const { AdminDroneService } = require("../../services/admin_drone_service")
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject");
const validateDroneUpdateData = require ("../../helpers/validateDroneUpdateData");
const Drone = new AdminDroneService()

async function update_drone_property(req, res, next) {

    const { update } = req.body;
    const id = req.params.droneId
    if(!id){
        next(ApiError.badRequest(`No id in params was sent`))
        return
    }
    const checks = validateDroneUpdateData(update);

    if (checks?.error) {
        next(ApiError.badRequest(checks?.message));
        return
    }
    const newProps = {}
    Object.keys(update).forEach(element => {
        if (update?.[element]) {
            newProps[element] = update[element]
        }
    })
    if (!Object.keys(newProps)?.length) {
        next(ApiError.badRequest(`No data was sent`))
        return
    }
    try {
        const updateResponse = await Drone.updateDrone({ id, newProps })
        return res.status(200).json(successObject({ data: updateResponse, code: 200, message: "Drone record updated successfully" }))
    } catch (err) {
        return res.sendStatus(500)
    }
}
module.exports.update_drone_property = update_drone_property