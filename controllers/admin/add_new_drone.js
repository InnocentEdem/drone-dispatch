const { AdminDroneService } = require("../../services/admin_drone_service");
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkModelName");
const ApiError = require("../../error/ApiError");
const validateNewDroneData = require("../../helpers/validateNewDroneData");
const successObject = require("../../helpers/successObject")

const Drone = new AdminDroneService();

async function add_new_drone(req, res, next) {
    const newDrone = req?.body?.newDrone;
    const checks = validateNewDroneData(newDrone);

    if (checks?.error) {
        next(ApiError.badRequest(checks?.message));
        return
    }
    try {
        const data = await Drone.createNewDrone(newDrone);
        return res.status(201).json(successObject({data:data?.Drone?.dataValues,code:201, message:"Drone created"}))
    } catch (err) {
        return res.sendStatus(500);
    }
}
module.exports.add_new_drone = add_new_drone;
