const bcrypt = require("bcrypt")
const {UserService} = require("../../services/user_service")
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject")
const User = new UserService()

const create_new_user = async (req, res, next) => {
    const roles = ["admin","dispatcher","basic"]

    const { email, password, role } = req.body
    if (!email || !password || !role) {
        next(ApiError.badRequest(`Some fields are missing values`))
        return
    }
    if(!roles.includes(role)){
        next(ApiError.badRequest(`Role must be one of ${roles}`))
        return
    }
    const hashedPassword = await bcrypt.hash(password, 10)
    try {
        const newUser = await User.createNewUser({ email, password: hashedPassword, role })
        if (!newUser?.created) {
            next(ApiError.badRequest(`User with Email already exists`))
            return 
        }
        return res.status(200).json(successObject({ message:`User: ${newUser?.email} Role: ${newUser?.role}`}))
    } catch (err) {
        res.status(401).send(err)
    }
}
module.exports.create_new_user = create_new_user