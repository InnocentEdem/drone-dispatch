const {BasicUserService} = require("../../services/basic_user_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject")
const db = new BasicUserService()

async function find_booked_missions(_req, res, _next){

    try{
       const response = await db.findBookedMissions()
       console.log(response);
       return res.json(successObject({ data:response, message: "Booked missions" }))
    }catch(err){
        return res.sendStatus(500)
    }
}
module.exports.find_booked_missions = find_booked_missions