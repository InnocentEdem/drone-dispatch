const {BasicUserService} = require("../../services/basic_user_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const validateMedicationUpdate= require("../../helpers/validateMedicationUpdate");
const successObject = require ("../../helpers/successObject");

const db = new BasicUserService()

async function update_medication (req, res, next){

    const update = req?.body?.update;
    const id = req.params.id
    
    if(!id){
        next(ApiError.badRequest("Medication id missing in params"));
        return
    }
    const checks = validateMedicationUpdate(update);

    if (checks?.error) {
        next(ApiError.badRequest(checks?.message));
        return
    }
    
    try{
       const updateResponse = await db.updateMedication({id,newProps:{...update}})
       return res.status(200).json(successObject({ data: updateResponse, code: 200, message: "Update medication record successful" }))

    }catch(err){
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.update_medication = update_medication