const {BasicUserService} = require("../../services/basic_user_service")
const successObject = require("../../helpers/successObject")
const ApiError = require("../../error/ApiError")
const db = new BasicUserService()

async function loaded_medication (req, res, next){

    const id = req?.params?.droneId
    if(!id){
        next(ApiError.badRequest(`No Mission ID was sent`))
        return 
    }
    try{
       const response = await db.getDroneLoadedMedication(id)
       return res.status(200).json(successObject({ data: response, code: 200, message: "Get Drone Loaded Medication" }))
    }catch(err){
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.loaded_medication = loaded_medication