const { BasicUserService } = require("../../services/basic_user_service");
const ApiError = require("../../error/ApiError");
const successObject = require("../../helpers/successObject");
const db = new BasicUserService();

async function drone_booked_missions(req, res, next) {
  const id = req.params.droneId;
  if (!id) {
    next(ApiError.badRequest(`No Drone ID was sent with request`));
    return;
  }
  try {
    const response = await db.findDroneBookedMissions( id );
    return res
      .status(200)
      .json(
        successObject({
          data: response,
          code: 200,
          message: "Fetch successful",
        })
      );
  } catch (err) {
    return res.sendStatus(500);
  }
}
module.exports.drone_booked_missions = drone_booked_missions;
