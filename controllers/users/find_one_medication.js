const { BasicUserService } = require("../../services/basic_user_service");
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError");
const successObject = require("../../helpers/successObject");

const db = new BasicUserService();

async function find_one_medication(req, res, next) {
  const id = req?.params.id;
  if (id === null) {
    next(ApiError.badRequest(`Request has no medication id`));
    return;
  }
  try {
    const data = await db.findOneMedication({ id });
    if (!data) return res.sendStatus(404);
    return res.json(successObject({ data, message: "fetch successful" }));
  } catch (err) {
    return res.sendStatus(500);
  }
}
module.exports.find_one_medication = find_one_medication;
