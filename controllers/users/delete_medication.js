const { BasicUserService } = require("../../services/basic_user_service");
const ApiError = require("../../error/ApiError");
const db = new BasicUserService();

async function delete_medication(req, res, next) {
  const id = req.params.medicationId;
  if (!id) {
    next(ApiError.badRequest(`No Medication ID was sent`));
    return;
  }
  try {
    const updateResponse = await db.deleteMedication({ id });
    return res.status(200).json(updateResponse);
  } catch (err) {
    return res.sendStatus(500);
  }
}
module.exports.delete_medication = delete_medication;
