const {BasicUserService} = require("../../services/basic_user_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject")
const db = new BasicUserService()

async function find_all_medication (_req, res, _next){

    try {
        const data = await db.findAllMedication()
        return res.json(successObject({ data, message: "fetch successful" }))
    } catch (err) {
        return res.sendStatus(500)
    }
}
module.exports.find_all_medication = find_all_medication