const {BasicUserService} = require("../../services/basic_user_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const successObject = require("../../helpers/successObject")
const ApiError = require("../../error/ApiError")
const db = new BasicUserService()

async function find_one_mission (req, res, next){

    const id = req?.params?.id
    if(!id){
        next(ApiError.badRequest(`No Mission ID was sent`))
        return 
    }
    try{
       const response = await db.getOneMission(id)
       return res.status(200).json(successObject({ data: response, code: 200, message: "Get one mission successful" }))
    }catch(err){
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.find_one_mission = find_one_mission