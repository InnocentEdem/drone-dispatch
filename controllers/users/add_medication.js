const { checkMedicationCode } = require("../../helpers/checkMedicationCode");
const { BasicUserService } = require("../../services/basic_user_service");
const validateMedication = require("../../helpers/validateMedication");
const ApiError = require("../../error/ApiError");
const successObject = require("../../helpers/successObject");
const db = new BasicUserService();

async function add_medication(req, res, next) {
  const medication = req?.body?.medication;
  let { imageName } = req?.body || { imageName: "default.png" };
  if (!imageName) {
    imageName = "default.png";
  }

  const checks = validateMedication(medication);

  if (checks?.error) {
    next(ApiError.badRequest(checks?.message));
    return;
  }

  try {
    const result = await db.createNewMedication({ ...medication, imageName });
    return res
      .status(201)
      .json(
        successObject({ data: result, code: 201, message: "Medication added" })
      );
  } catch (err) {
    return res.sendStatus(500);
  }
}
module.exports.add_medication = add_medication;
