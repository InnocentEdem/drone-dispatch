const { DispatchDroneService } = require("../../services/dispatcher_drone_service")
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject");
const db = new DispatchDroneService()

async function drone_post_launch(req, res, next) {

    const preLaunchStates = ['IDLE', 'LOADING', 'LOADED',]
    const postLaunchStates = ['DELIVERED', 'RETURNING','IDLE']
    const launchState = ['DELIVERING']

    const id = req?.params?.droneId;
    const update = req?.body?.update

    if (!id || !update?.state) {
        next(ApiError.badRequest(`No Drone ID or Update object  available in request`))
        return
    }
    update.state = update.state.toUpperCase()
    if (!postLaunchStates.includes(update?.state)) {
        next(ApiError.badRequest(`Post launch states can only be ${[postLaunchStates]}`))
        return
    }
    try {
        const drone = await db.getOneDrone({ id })
        const droneState = drone.toJSON().state

        if (preLaunchStates.includes(droneState)) {
            next(ApiError.badRequest(`Drone has not been launched. Current state is ${droneState} `))
            return
        }
        if (droneState == update.state) {
            next(ApiError.badRequest(`Drone  is already in [${droneState}]  state `))
            return
        }
        const response = await db.updateDroneState({ id, state: update?.state })
        return res.status(200).json(successObject({ data: response, code: 200, message: `Drone state updated to ${update.state}` }))

    } catch (err) {
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.drone_post_launch = drone_post_launch