const {DispatchDroneService} = require("../../services/dispatcher_drone_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject =require("../../helpers/successObject");
const db = new DispatchDroneService()

async function update_drone_state (req, res, next){

    const states = ['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING']

    const id = req.params.id;
    const update = req?.body?.update;
    if(!update?.state){
        next(ApiError.badRequest(`No state data present in request body`))
        return 
    }
    update.state = update.state.toUpperCase()

    if(!states.includes(update.state)){
        next(ApiError.badRequest(`State must be one of ${states}`))
        return 
    }
    if(!id){
        next(ApiError.badRequest(`No Drone ID was sent`))
        return 
    }
    try{
       const response = await db.updateDroneState({id,...update})
       return res.status(200).json(successObject({ data: response, code: 200, message: "Drone state update successfull" }))

    }catch(err){
        return res.sendStatus(500)
    }
}
module.exports.update_drone_state  = update_drone_state