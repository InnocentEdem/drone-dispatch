const { DispatchDroneService } = require("../../services/dispatcher_drone_service");
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject");

const db = new DispatchDroneService()

async function get_loading_ready (req, res, next){
    try{
       const response = await db.getReadyToLoad()
       return res.status(200).json(successObject({ data: response, code: 200, message: "Get Drones available to Load" }))
    }catch(err){
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.get_loading_ready = get_loading_ready