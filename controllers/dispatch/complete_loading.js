const {DispatchDroneService} = require("../../services/dispatcher_drone_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject =require("../../helpers/successObject");
const db = new DispatchDroneService()

async function complete_loading (req, res, next){

    const states = ['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING']

    const id = req.params.droneId;

    if(!id){
        next(ApiError.badRequest(`No Drone ID was sent`))
        return 
    }
    try{
        const drone =  await db.getOneDrone({id})
        const droneState = drone.toJSON().state
        console.log(droneState);
        if(droneState !== "LOADING"){
            next(ApiError.badRequest(`Drone is not Loading currently. `))
            return 
        }
       const response = await db.updateDroneState({id,state:"LOADED"})
       return res.status(200).json(successObject({ data: response, code: 200, message: "Drone state update successfull" }))

    }catch(err){
        return res.sendStatus(500)
    }
}
module.exports.complete_loading  = complete_loading