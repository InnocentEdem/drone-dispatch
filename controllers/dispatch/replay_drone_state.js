const {DispatchDroneService} = require("../../services/dispatcher_drone_service")
const ApiError = require("../../error/ApiError")
const successObject =require("../../helpers/successObject");
const db = new DispatchDroneService()

async function replay_drone_state (req, res, next){

    const id = req?.params?.id
    const registration = req?.params?.registration
    if(!id && !registration){
        next(ApiError.badRequest(`Drone id or Registration required`))
        return 
    }
    try{
       const response = await db.droneStatesReplay({id,registration})
       return res.status(200).json(successObject({ data: response, code: 200, message: "Drone State Change events" }))

    }catch(err){
        return res.sendStatus(500)
    }
}
module.exports.replay_drone_state = replay_drone_state