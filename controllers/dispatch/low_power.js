const { DispatchDroneService } = require("../../services/dispatcher_drone_service");
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject");

const db = new DispatchDroneService()

async function low_power (_req, res, _next){
    try{
       const response = await db.getLowPower()
       return res.status(200).json(successObject({ data: response, code: 200, message: "Get Drones with low power" }))
    }catch(err){
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.low_power = low_power