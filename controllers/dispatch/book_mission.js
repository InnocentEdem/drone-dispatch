const { DispatchDroneService } = require("../../services/dispatcher_drone_service");
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkModelName");
const ApiError = require("../../error/ApiError");
const validateMission = require("../../helpers/validateMission");
const successObject = require("../../helpers/successObject");
const checkMissionWeight = require("../../helpers/checkMissionWeight");

const db = new DispatchDroneService();

async function book_mission(req, res, next) {

    let mission = req?.body?.mission;
    const checks = validateMission(mission);

    if (checks?.error) {
        next(ApiError.badRequest(checks?.message));
        return
    }
    const {drone_id} = mission
    mission = { ...mission, current_status: "BOOKED" }
    try {
        const payload = mission?.payload;
        if(!payload?.length){
            next(ApiError.badRequest(`Mission Payload is empty`));
            return
        }
        const medicationData = await db.fetchMedicationData({data:payload})
        const droneData = await db.getOneDrone({id:drone_id})
        const weightCheck = checkMissionWeight(medicationData, droneData, payload)

        if (weightCheck?.error) {
            next(ApiError.badRequest(weightCheck?.message));
            return
        }
        const data = await db.createNewMission(mission);
        return res.status(200).json(successObject({ data, message: "Mission Booked" }))
    } catch (err) {
        console.log(err);
        return res.sendStatus(500);
    }
}
module.exports.book_mission = book_mission;