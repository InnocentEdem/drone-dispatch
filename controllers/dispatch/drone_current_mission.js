const {DispatchDroneService} = require("../../services/dispatcher_drone_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject =require("../../helpers/successObject");
const db = new DispatchDroneService()

async function drone_current_mission (req, res, next){

    const states = ['IDLE', 'LOADING', 'LOADED', 'DELIVERING', 'DELIVERED', 'RETURNING']

    const id = req.params.droneId;

    if(!id){
        next(ApiError.badRequest(`No Drone ID was sent`))
        return 
    }
    try{
       const response = await db.getDroneCurrentMission(id)
       return res.status(200).json(successObject({ data: response, code: 200, message: "Drone current mission" }))

    }catch(err){
        return res.sendStatus(500)
    }
}
module.exports.drone_current_mission  = drone_current_mission