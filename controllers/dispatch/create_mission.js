const { DispatchDroneService } = require("../../services/dispatcher_drone_service");
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkModelName");
const ApiError = require("../../error/ApiError");
const validateMission = require("../../helpers/validateMission");
const successObject = require("../../helpers/successObject");
const checkMissionWeight = require("../../helpers/checkMissionWeight");

const db = new DispatchDroneService();

async function create_mission(req, res, next) {

    let mission = req?.body?.mission;


    const drone_id = mission?.drone_id
    mission = { ...mission, current_status: "LOADING" }
    if(!drone_id){
        next(ApiError.badRequest(`Drone id missing.`));
        return
    }
    try {
        const payload = mission?.payload;
        const droneData = await db.getOneDrone({id:drone_id})
        const medicationData = await db.fetchMedicationData({data:payload})
        const weightCheck = checkMissionWeight(medicationData, droneData, payload)
        const statusCheck = droneData.toJSON()?.state
        const battery = droneData.toJSON()?.batteryCapacity

        if (statusCheck !== "IDLE") {
            next(ApiError.badRequest(`Selected Drone current state is ${statusCheck}. Loading cannot proceed`));
            return
        }

        await db.updateDroneState({state:"LOADING",id: drone_id})

        const checks = validateMission(mission);

        if (checks?.error) {
            next(ApiError.badRequest(checks?.message));
            return
        }

        if(!payload?.length){
            next(ApiError.badRequest(`Mission Payload is empty`));
            return
        }
        if (weightCheck?.error) {
            next(ApiError.badRequest(weightCheck?.message));
            return
        }

        if (+battery < 25) {
            next(ApiError.badRequest(`Battery Level is Critical: [ ${battery}% ]. Loading cannot proceed`));
            return
        }

        const data = await db.createNewMission(mission);
        return res.status(201).json(successObject({ data, code:201, message: "Mission created" }))
    } catch (err) {
        console.log(err);
        return res.sendStatus(500);
    }
}
module.exports.create_mission = create_mission;