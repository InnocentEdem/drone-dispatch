const {DispatchDroneService} = require("../../services/dispatcher_drone_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject =require("../../helpers/successObject");
const db = new DispatchDroneService()

async function launch_drone (req, res, next){

    const id = req.params.droneId;
    console.log("launching");

    if(!id){
        next(ApiError.badRequest(`No Drone ID was sent`))
        return 
    }
    try{
        const drone =  await db.getOneDrone({id})
        const droneState = drone.toJSON().state
        console.log(droneState);
        if(droneState !== "LOADED"){
            next(ApiError.badRequest(`Drone is not ready for Launch. Current state is ${droneState} `))
            return 
        }
       const response = await db.updateDroneState({id,state:"DELIVERING"})
       return res.status(200).json(successObject({ data: response, code: 200, message: "Drone state update successfull" }))

    }catch(err){
        return res.sendStatus(500)
    }
}
module.exports.launch_drone  = launch_drone