const { DispatchDroneService } = require("../../services/dispatcher_drone_service")
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject");
const { BasicUserService } = require("../../services/basic_user_service")
const db = new DispatchDroneService()
const basicdb = new BasicUserService()

async function load_drone(req, res, next) {

    const mission_id = req?.body?.mission_id

    if (!mission_id) {
        next(ApiError.badRequest(`Mission id is required`))
        return
    }
    try {
        const mission = await basicdb.getOneMission(mission_id)
        const missionStatus = mission?.toJSON()?.current_status
        if (missionStatus !== "BOOKED") {
            next(ApiError.badRequest(`Mission is not Available for loading`))
            return
        }
        const droneId = mission.toJSON()?.drone_id
        const drone = await db.getOneDrone({ id: droneId })
        const droneState = drone.toJSON()?.state
        const droneBatteryState = drone.toJSON()?.batteryCapacity

        if (droneState !== "IDLE") {
            next(ApiError.badRequest(`Drone current state is ${droneState}. Drone is unavailable for loading`))
            return
        }
        if (+droneBatteryState < 25) {
            next(ApiError.badRequest(`Drone battery is ${droneBatteryState}%. Drone is unavailable for loading`))
            return
        }
        await db.updateDroneState({ state: "LOADING", id: droneId })
        basicdb.updateMissionState({id:mission_id,status:"LOADING"})
        return res.json(successObject({ data: mission, message: "Drone state moved to loading. " }))
    } catch (err) {
        console.log(err);
    }
}
module.exports.load_drone = load_drone