const { DispatchDroneService } = require("../../services/dispatcher_drone_service");
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkModelName");
const ApiError = require("../../error/ApiError");
const validateNewDroneData = require("../../helpers/validateNewDroneData");
const successObject = require("../../helpers/successObject")

const Drone = new  DispatchDroneService();

async function get_all_drones(req, res, next) {

    try {
        const data = await Drone.getAllDrones();
        const filtered = data.map(element=>element.dataValues)
        return res.status(200).json(successObject({ data: filtered, code: 200, message: "fetch all records successful" }))
    } catch (err) {
        console.error(err);
        return res.sendStatus(500);
    }
}
module.exports.get_all_drones = get_all_drones;