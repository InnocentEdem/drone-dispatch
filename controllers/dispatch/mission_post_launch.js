const { DispatchDroneService } = require("../../services/dispatcher_drone_service")
const { BasicUserService } = require("../../services/basic_user_service")
const checkWeightLimit = require("../../helpers/checkWeightLimit");
const checkModelName = require("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject = require("../../helpers/successObject");
const db = new DispatchDroneService()
const missionDb = new BasicUserService()

async function mission_post_launch(req, res, next) {

    const preLaunchStates = ['BOOKED', 'LOADING', 'LOADED']
    const postLaunchStates = ['DELIVERED', 'LOST','RETURNED','DESTROYED']
    const launchState = ['DELIVERING']

    const id = req?.params?.missionId;
    const update = req?.body?.update

    if (!id || !update?.state) {
        next(ApiError.badRequest(`No Mission ID or Update object is available in request`))
        return
    }
    if (!postLaunchStates.includes(update?.state)) {
        next(ApiError.badRequest(`Post launch states can only be one of ${[postLaunchStates]}`))
        return
    }
    update.state = update.state.toUpperCase()

    try {
        const mission = await db.getOneMission(id)
        const missionState = mission.toJSON().current_status
        console.log(update.state,"mission");

        if (preLaunchStates.includes(missionState)) {
            next(ApiError.badRequest(`Mission has not started. Current state is ${missionState} `))
            return
        }
        if (missionState == update.state) {
            next(ApiError.badRequest(`Mission  is already in [${missionState}]  state `))
            return
        }
        const response = await missionDb.updateMissionState({ id, status: update.state })
        return res.status(200).json(successObject({  code: 200, message: `Mission state updated to ${update.state}` }))

    } catch (err) {
        console.log(err);
        return res.sendStatus(500)
    }
}
module.exports.mission_post_launch = mission_post_launch