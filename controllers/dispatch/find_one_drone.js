const {DispatchDroneService} = require("../../services/dispatcher_drone_service")
const checkWeightLimit  = require ("../../helpers/checkWeightLimit");
const checkModelName  = require ("../../helpers/checkWeightLimit");
const ApiError = require("../../error/ApiError")
const successObject =require("../../helpers/successObject");
const db = new DispatchDroneService()

async function find_one_drone (req, res, next){

    const id = req?.params?.id
    const registration = req?.params?.registration

    if(!id && !registration){
        next(ApiError.badRequest(`No Drone ID or Registration was sent`))
        return 
    }
    try{
        const response = await db.getOneDrone({id,registration})
        return res.status(200).json(successObject({ data: response.dataValues, code: 200, message: "Drone fetch successfull" }))

    }catch(err){
        return res.sendStatus(500)
    }
}
module.exports.find_one_drone  = find_one_drone