const {Drone,BatteryLevelLog} = require("../models")
const {DroneStateEvents} = require("../models");


class AdminDroneService {

    async createNewDrone(data){

        const{model,weightLimit,batteryCapacity,state,registration} = data;
        try{
            return await Drone.findOrCreate({
                where:{registration},
                defaults: {
                    model,
                    weight_limit: weightLimit,
                    battery_capacity:batteryCapacity,
                    state,
                    registration
                }
            })
        }
        catch(err){
            console.log(err)
        }
    }
    async updateDrone(data) {
        const { id, newProps } = data;
        const drone = await Drone.findOne({
            where: { id },
            attributes: ['model', 'weight_limit', 'registration', 'battery_capacity', 'state']
        })
        const values = { ...drone?.dataValues, ...newProps }
        try {
            return await Drone.upsert({
                id,
                ...values
            })
        }
        catch (err) {
            return err
        }
    }
    async droneEventsReplay(data){
        const{registration} = data;
        try{
            return await DroneStateEvents.findAll({
                where:{registration}
            })
        }
        catch(err){

        }
    }
    async generateLog( offset=0, limit = 50){
        try{
            return await BatteryLevelLog.findAll({
                attributes:["timestamp","battery_event"],
                limit,
                offset,
                order:[['timestamp',"DESC"]]
                
            })
        }catch(err){
            console.log(err)
        }
       
    }

}

module.exports.AdminDroneService = AdminDroneService