const { Drone, Mission, MissionMedication,Medication } = require("../models")
const { DroneStateEvents } = require("../models");
const { Op } = require("sequelize");
const medication = require("../models/medication");
const drone = require("../models/drone");


class DispatchDroneService {

  async createNewMission(data) {
    const { payload, ...rest } = data;
    try {
      const res = await Mission.create({
        ...rest,
      });
      const missionDrone = await Drone.findOne({
        where:{id: rest?.drone_id }
      })
      missionDrone.addMission(res)

      for (let element of payload) {
        const med = await Medication.findByPk(element.medication_id);
        await res.addMedication(med, {
          through: { quantity: element.quantity },
        });
      }
      const mission = res?.toJSON()?.id;
      const res3 = await Mission.findOne({
        where: { id: mission },
        include:[ {
          model: Medication,
          attributes:['id','code',"name"],
          through:{
            model: MissionMedication,
            as:"MedicationDetails",
            attributes:['quantity']
          }
        }]
      });
      return res3.toJSON();
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async updateDroneState(data) {
    const state = data?.state;
    const id = data?.id;
    try {
      const res = await Drone.findOne({
        where: { id },
        attributes: [
          "model",
          "id",
          "registration",
          "battery_capacity",
          "weight_limit",
          "state",
        ],
      });
      if (!res) {
        throw new Error("No record found for Drone with this id");
      }
      if(state === "LOADED"){
         await Mission.update({
          current_status:"LOADED"
        },{
          where:{
            [Op.and]:[{drone_id:id},{current_status:"LOADING"}]
          }
        }
        )
      }
      if(state === "DELIVERING"){
         await Mission.update({
          current_status:"DELIVERING"
        },{
          where:{
            [Op.and]:[{drone_id:id},{current_status:"LOADED"}]
          }
        }
        )
      }
      if(state === "DELIVERED"){
         await Mission.update({
          current_status:"DELIVERING"
        },{
          where:{
            [Op.and]:[{drone_id:id},{current_status:"DELIVERED"}]
          }
        }
        )
      }
      const values = { ...res.toJSON(), state };
      const res2 = await Drone.upsert({
        id,
        ...values,
      });
      await DroneStateEvents.create({
        timestamp: Date.now(),
        drone: id,
        event: state,
      });
      return res2;
    } catch (err) {
      console.log(err);
    }
  }
  async getAllDrones() {
    try {
      return await Drone.findAll({
        order: [["id", "ASC"]],
        attributes: [
          "id",
          ["weight_limit", "weightLimit"],
          ["battery_capacity", "batteryCapacity"],
          "state",
          "registration",
          "model",
        ],
      });
    } catch (err) {
      return err;
    }
  }
  async getOngoingMissions() {
    try {
      return await Mission.findAll({
        where:{
          current_status: {
            [Op.or]:["LOADING","LOADED","DELIVERING"]
          }
        },
      });
    } catch (err) {
      console.log(err);
      return err;
    }
  }
  async getDroneCurrentMission(id) {
    try {
      return await Mission.findAll({
        limit: 1,
        where:{
          current_status: {
            [Op.or]:["LOADING","LOADED","DELIVERING"]
          },
          drone_id:id
        },
        order:[['createdAt',"DESC"]]
      });
    } catch (err) {
      return err;
    }
  }

  async getOneDrone({ id,registration }) {
    try {
      if(id){
        return await Drone.findOne({
          where: { id },
          attributes: [
            "id",
            ["weight_limit", "weightLimit"],
            ["battery_capacity", "batteryCapacity"],
            "state",
            "registration",
          ],
        });
      }
      if(registration){
        return await Drone.findOne({
          where: { registration },
          attributes: [
            "id",
            ["weight_limit", "weightLimit"],
            ["battery_capacity", "batteryCapacity"],
            "state",
            "registration",
          ],
        });
      }

    } catch (err) {
      return err;
    }
  }

  async confirmDroneAvailable({ id }) {
    try {
      return await Drone.findOne({
        where: { id },
        attributes: ["state"],
      });
    } catch (err) {
      return err;
    }
  }

  async fetchMedicationData({ data }) {
    const pkArray = data?.map((element) => element.medication_id);
    try {
      return await Medication.findAll({
        where: { id: pkArray },
        attributes: ["name", "weight", "code", "id"],
      });
    } catch (err) {
      return err;
    }
  }

  async getReadyToLoad() {
    try {
      return await Drone.findAll({
        where: {
          [Op.and]:[
            {state:"IDLE"},
            {battery_capacity:{[Op.gte]:[25]}}
          ]
        },
        attributes: ["id","model", "weight_limit", "battery_capacity","state"]
      });
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async getLowPower() {
    try {
      return await Drone.findAll({
        where: {
          [Op.and]:[
            {state:"IDLE"},
            {battery_capacity:{[Op.lte]:[25]}}
          ]
        },
        attributes: ["id","model", "weight_limit", "battery_capacity","state"]
      });
    } catch (err) {
      console.log(err);
      return err;
    }
  }

  async droneStatesReplay(data) {
    const registration = data?.registration;
    const id = data?.id;

    try {
      if (registration) {
        return await DroneStateEvents.findAll({
          where: { registration },
        });
      }
      if (id) {
        return await DroneStateEvents.findAll({
          where: { drone:id },
          attributes:[ "timestamp",["drone","drone_id"],"event"]
        });
      }
    } catch (err) {
      return err;
    }
  }
  async getOneMission (id){
    try{
      return await Mission.findOne({
        where: { id },
        include:[ {
          model: Medication,
          attributes:['id','code',"name"],
          through:{
            model: MissionMedication,
            as:"MedicationDetails",
            attributes:['quantity']
          }
        }]
      });
    }catch(err){
      console.log(err);
    }

  }
}

module.exports.DispatchDroneService = DispatchDroneService