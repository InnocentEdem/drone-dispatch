
const {BatteryLevelLog,Drone} = require("../models")


class CronService {

    async saveEvent(event){
        const{timestamp,battery_event} = event;
        try{
            return await BatteryLevelLog.create({
                timestamp,
                battery_event
            })

        }
        catch(err){
            console.log(err);
            return err
        }

    }
    async generateLog( offset=0, limit = 50){
        try{
            return await BatteryLevelLog.findAll({
                attributes:["timestamp","battery_event"],
                limit,
                offset
            })
        }catch(err){
            console.log(err)
        }
       
    }
    async getBatteriesLatestState(){

        try{
            return await Drone.findAll({
                attributes:[["id","drone_id"],"registration","battery_capacity"],
            })
        }catch(err){
            console.log(err);

        }
       
    }

}
module.exports.CronService = CronService