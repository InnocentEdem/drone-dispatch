const { Medication, Mission,MissionMedication, Drone } = require("../models");
const { Op } = require("sequelize");


class BasicUserService {
  async createNewMedication(medication) {
    const { name, weight, code, imageName } = medication;
    try {
      return await Medication.findOrCreate({
        where: { code, name },
        defaults: {
          name,
          code,
          weight,
          image: imageName,
        },
      });
    } catch (err) {
      return err;
    }
  }
  async updateMedication(data) {
    const { id, newProps } = data;
    const medication = await Medication.findOne({
      where: { id },
      attributes: ["name", "weight", "image", "code"],
    });
    const values = { ...medication?.dataValues, ...newProps };

    try {
      return await Medication.upsert({
        id,
        ...values,
      });
    } catch (err) {
      return err;
    }
  }
  async deleteMedication(data) {
    const { id } = data;
    try {
      return await Medication.destroy({
        where: {
          id,
        },
      });
    } catch (err) {
      return err;
    }
  }
  async findOneMedication(data) {
    const { id } = data;
    try {
      return await Medication.findOne({
        where: {
          id,
        },
        attributes: ["id", "code", "image", "weight"],
      });
    } catch (err) {
      return err;
    }
  }
  async findAllMedication(_data) {
    try {
      return await Medication.findAll({
        order: [["code", "ASC"]],
        attributes: ["id", "name", "code", "image", "weight"],
      });
    } catch (err) {
      return err;
    }
  }
  async findAllMission(_data) {
    try {
      return await Mission.findAll({
        order: [["id", "ASC"]],
        attributes: [
          "id",
          "drone_id",
          "current_status",
          "title",
          "description",
          "location",
          
        ],
        include: [
          {
            model: Medication,
            attributes:["id","name","code"],
            through:{
              attributes:["quantity"]
            }
          },
        ],
      });
    } catch (err) {
      return err;
    }
  }
  async getOneMission (id){
    try{
      return await Mission.findOne({
        where: { id },
        include:[ {
          model: Medication,
          attributes:['id','code',"name"],
          through:{
            model: MissionMedication,
            as:"MedicationDetails",
            attributes:['quantity']
          }
        }]
      });
    }catch(err){
      console.log(err);
    }

  }
  async updateMissionState ({id,status}){
    try{
      return await Mission.update({
        current_status:status
      },{
        where:{
          id
        }
      }
      )
    }catch(err){
      console.log(err);
    }

  }
  async findDroneMissions(data) {
    const { id } = data;
    try {
      return await Mission.findAll({
        where: { drone_id: id },
        attributes: [
          `id`,
          `location`,
          `title`,
          `description`,
          `current_status`,
          `drone_id`,
        ],
        include:[ {
          model: Medication,
          attributes:['id','code',"name"],
          through:{
            model: MissionMedication,
            as:"MedicationDetails",
            attributes:['quantity']
          }
        }]
      });
    } catch (err) {
      console.log(err);
      return err;
    }
  }
  async getDroneLoadedMedication (id){
    try{
      return await Mission.findOne({
        where: {[Op.and]:[{drone_id: id},{current_status:"LOADED"}]},
        attributes:[],
        include:[ {
          model: Medication,
          attributes:['id','code',"name"],
          through:{
            model: MissionMedication,
            as:"MedicationDetails",
            attributes:['quantity']
          }
        }]
      });
    }catch(err){
      console.log(err);
    }

  }
  async findBookedMissions(_data) {
    try {
      return await Mission.findAll({
        where:{current_status:"BOOKED"},
        order: [["id", "ASC"]],
        attributes: [
          "id",
          "drone_id",
          "current_status",
          "title",
          "description",
          "location",
          
        ],
        include: [
          {
            model: Medication,
            attributes:["id","name","code"],
            through:{
              attributes:["quantity"]
            }
          },
        ],
      });
    } catch (err) {
      return err;
    }
  }
  async findDroneBookedMissions(droneId) {
    try {
      return await Mission.findAll({
        where:{[Op.and]:[{current_status:"BOOKED"},{drone_id:droneId}]},
        order: [["id", "ASC"]],
        attributes: [
          "id",
          "drone_id",
          "current_status",
          "title",
          "description",
          "location",
          
        ],
        include: [
          {
            model: Medication,
            attributes:["id","name","code"],
            through:{
              attributes:["quantity"]
            }
          },
        ],
      });
    } catch (err) {
      return err;
    }
  }
}
module.exports.BasicUserService = BasicUserService;
