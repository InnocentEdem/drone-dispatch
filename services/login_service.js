
const { User } = require("../models")

class LoginService {

    async loginUser(data) {
        const { email} = data;
        try {
            return await User.findOne({
                where: {
                    email,
                },
                attributes:["password","role"]
            })
        }
        catch (err) {
            console.log(err);
            return err
        }
    }
}
module.exports.LoginService = LoginService
