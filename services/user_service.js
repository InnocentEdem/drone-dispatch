
const {User} = require("../models")


class UserService {

    async createNewUser(user){
        const{email,password,role} = user;
        try{
            const [row, created] = await User.findOrCreate({
                where:{email},
                defaults: {
                    role, password,email
                },
            })
            return {email: row?.toJSON()?.email, 
                    role:  row?.toJSON()?.role,
                    created
                    }
        }
        catch(err){
            console.log(err);
            return err
        }

    }
    async resetUserPassword(user){
        const{email,newPassword} = user; 
        try{
            await User.update({password:newPassword},{
                where: {
                    email
                }
            })
        }catch(err){
            return err
        }
    }
    async resetUserRole(user){
        const{email,newRole} = user; 
        try{
            await User.update({role:newRole},{
                where: {
                    email
                }
            })
        }catch(err){
        }
    }
}
module.exports.UserService = UserService