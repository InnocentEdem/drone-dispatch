module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Drones', [{
      model: 'Lightweight',
      registration: '9G 009',
      weight_limit: 200,
      battery_capacity:80,
      state:"IDLE",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

