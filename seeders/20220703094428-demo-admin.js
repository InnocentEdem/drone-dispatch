'use strict';
const bcrypt = require("bcrypt")


module.exports = {
  async up (queryInterface, Sequelize) {
     
     await queryInterface.bulkInsert('Users', [{
        email: 'johndoe@dronemail.com',
        role: 'admin',
        password:  bcrypt.hashSync('Kofi1234', 10),
        createdAt: new Date(),
        updatedAt: new Date()
      }], {});
    
  },

  async down (queryInterface, Sequelize) {

      await queryInterface.bulkDelete('Users', null, {});
  }
};
