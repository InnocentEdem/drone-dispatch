module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Drones', [{
      model: 'Heavyweight',
      registration: '9G 008',
      weight_limit: 500,
      battery_capacity:10,
      state:"IDLE",
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
