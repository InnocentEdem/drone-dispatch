const CronJob = require("node-cron");
const { CronService } = require("../services/cron_service");
const db = new CronService();

module.exports = async () => {
  const scheduledBatteryCheckup = CronJob.schedule("* * * * *", async () => {
    const timestamp = Date.now();
    const res = await db.getBatteriesLatestState();
    const battery_event = JSON.stringify(
      res.map((element) => element.dataValues)
    );
    await db.saveEvent({ battery_event, timestamp });
  });
  scheduledBatteryCheckup.start();
};
